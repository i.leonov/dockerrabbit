<?php

require_once __DIR__ . '/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;

$database = new PDO('mysql:host=mariadb;dbname=logs', 'root', 'root');

$connection = new AMQPStreamConnection('rabbitmq', 5672, 'guest', 'guest');
$channel = $connection->channel();

$channel->queue_declare("test2", false, true, false, false);

echo " [*] Waiting for logs. To exit press CTRL+C\n";

$callback = function ($msg) use ($database){
    echo $msg->body, "\n";
    $data = explode(' ', $msg->body, 2);
    $queue = 'INSERT INTO Messages (message, email) VALUES (?, ?)';
    $database->prepare($queue)->execute([$data[1], $data[0]]);
};

while (true) {
    sleep(1);
    if (($counter = $channel->queue_declare("test2", true, true, false, false)[1]) >= 5) {
        $tag = $channel->basic_consume('test2', '', false, true, false, false, $callback);
        while($counter--) {
            $channel->wait();
        }
        $channel->basic_cancel($tag);
    }
}

$database = null;
$channel->close();
$connection->close();
