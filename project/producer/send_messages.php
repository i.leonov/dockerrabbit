<?php

require_once __DIR__ . '/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

try {
    $redis = new \Predis\Client([
        'scheme' => 'tcp',
        'host' => 'redis',
        'port' => 6379]);
} catch (Exception $exception) {
    die($exception->getMessage());
}

$handler = new \Predis\Session\Handler($redis);
session_set_save_handler($handler);

session_start();

if(!isset($_SESSION['email'])) {
    header('Location: /producer/');
}

echo $redis->get('test');

if (isset($_POST['message']) && $_POST['message']) {
    $connection = new AMQPStreamConnection('rabbitmq', 5672, 'guest', 'guest');
    $channel = $connection->channel();

    $channel->queue_declare("test2", false, true, false, false);

    $data = $_POST['message'];

    $msg = new AMQPMessage(
        $_SESSION['email'] . ' ' . $data,
        array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT));

    $channel->basic_publish($msg, '', 'test2');

    echo 'Sent ', $data, "\n";

    $channel->close();
    $connection->close();
}
?>
<form action="send_messages.php" method="post">
    <label for="message">Message</label>
    <input name="message" type="text" placeholder="Message" id="message" autofocus>
    <input type="submit" value="Send">
</form>
