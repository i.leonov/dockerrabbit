<?php
require_once __DIR__ . '/vendor/autoload.php';

try {
    $redis = new \Predis\Client([
        'scheme' => 'tcp',
        'host' => 'redis',
        'port' => 6379]);
} catch (Exception $exception) {
    die($exception->getMessage());
}

$handler = new \Predis\Session\Handler($redis);
session_set_save_handler($handler);

session_start();

if(isset($_SESSION['email'])) {
    header('Location: /producer/send_messages.php');
}

if(isset($_POST['email']) && $_POST['email']) {
    $_SESSION['email'] = $_POST['email'];
    header('Location: /producer/send_messages.php');
}
?>
<form action="login.php" method="post">
    <label for="email">Email</label>
    <input name="email" type="email" placeholder="Email" id="email" autofocus>
    <input type="submit" value="Login">
</form>
